package com.pa.spring.SpringBoard.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Assignment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false, unique = true)
    private long id;
    private String name;

    private Assignment() {
    }

    public Assignment(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public boolean equals(Object other) {
        return this == other
                || other != null
                && other.getClass() == Assignment.class
                && this.equals((Assignment) other);
    }

    private boolean equals(Assignment other) {
        return Objects.equals(id, other.id) &&
                Objects.equals(name, other.name);
    }

    @Override
    public String toString() {
        return "Assignment{" +
                "getId=" + id +
                ", getName='" + name + '\'' +
                '}';
    }
}