package com.pa.spring.SpringBoard.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Consultant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false, unique = true)
    private long id;
    private String fullName;
    private String email;
    @ManyToOne
    private Assignment assignment;
    @ManyToMany
    private List<Skill> skills;

    public Consultant(String fullName, String email, Assignment assignment, List<Skill> skills) {
        this.fullName = fullName;
        this.email = email;
        this.assignment = assignment;
        this.skills = skills;
    }

    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

    public boolean isAvailable() {
        return assignment == null;
    }

    public List<Skill> getSkills() {
        return Collections.unmodifiableList(skills);
    }

    public Assignment getAssignment() {
        return assignment;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fullName, email);
    }

    @Override
    public boolean equals(Object other) {
        return this == other
                || other != null
                && other.getClass() == Consultant.class
                && this.equals((Consultant) other);
    }

    private boolean equals(Consultant other) {
        return Objects.equals(id, other.id) &&
                Objects.equals(fullName, other.fullName) &&
                Objects.equals(email, other.email);
    }

    @Override
    public String toString() {
        return "Consultant{" +
                "getId=" + id +
                ", getFullName='" + fullName + '\'' +
                ", getEmail='" + email + '\'' +
                ", assignment=" + assignment +
                ", getSkills=" + skills +
                '}';
    }
}