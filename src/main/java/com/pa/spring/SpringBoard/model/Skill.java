package com.pa.spring.SpringBoard.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Skill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false, unique = true)
    private long id;
    @Column(unique = true)
    private String name;
    private String description;

    private Skill() {
    }

    public Skill(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return this.description;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }

    @Override
    public boolean equals(Object other) {
        return this == other
                || other != null
                && other.getClass() == Skill.class
                && this.equals((Skill) other);
    }

    private boolean equals(Skill other) {
        return Objects.equals(id, other.id) &&
                Objects.equals(name, other.name) &&
                Objects.equals(description, other.description);
    }

    @Override
    public String toString() {
        return "Skill{" +
                "getId=" + id +
                ", getName='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}