package integration.pa.spring.SpringBoard.repository;

import com.pa.spring.SpringBoard.SpringBoardApplication;
import com.pa.spring.SpringBoard.model.Assignment;
import com.pa.spring.SpringBoard.model.Consultant;
import com.pa.spring.SpringBoard.model.Skill;
import com.springboard.webapp.WebappApplication;
import com.springboard.webapp.model.Consultant;
import com.springboard.webapp.model.Skill;
import com.springboard.webapp.repository.ConsultantRepository;
import com.springboard.webapp.repository.SkillRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.Optional;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@DataJpaTest()
@ContextConfiguration(classes = SpringBoardApplication.class)
public class ConsultantRepositoryTest {
    private static final Skill JAVA = new Skill("Java", "Java programming language");
    @Autowired
    private ConsultantRepository consultantRepository;
    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void findBySkill() {
        final Consultant leo = new Consultant("Leo Wrest", "Leo.Wrest@paconsulting.com", null, Collections.singletonList(JAVA));
        entityManager.persist(leo);
        entityManager.flush();

        final Optional<Consultant> found = consultantRepository.findAllBySkillsContaining(JAVA);

        assertTrue(found.isPresent());
        assertEquals(leo.getFullName(), found.get().getFullName());
        assertEquals(leo.getEmail(), found.get().getEmail());
        assertNull(found.get().getAssignment());
        assertEquals(1, found.get().getSkills().size());
        assertEquals(JAVA.getName(), found.get().getSkills().get(0).getName());
        assertEquals(JAVA.getDescription(), found.get().getSkills().get(0).getDescription());
    }
}