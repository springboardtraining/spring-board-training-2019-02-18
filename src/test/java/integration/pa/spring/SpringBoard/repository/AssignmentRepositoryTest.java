package integration.pa.spring.SpringBoard.repository;

import com.pa.spring.SpringBoard.SpringBoardApplication;
import com.pa.spring.SpringBoard.model.Assignment;
import com.springboard.webapp.WebappApplication;
import com.springboard.webapp.model.Assignment;
import com.springboard.webapp.repository.AssignmentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest()
@ContextConfiguration(classes = SpringBoardApplication.class)
public class AssignmentRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AssignmentRepository assignmentRepository;

    @Test
    public void findByName() {
        // given
        final Assignment testAssignment = new Assignment("My Assignment");
        entityManager.persist(testAssignment);
        entityManager.flush();

        // when
        Optional<Assignment> found = assignmentRepository.findByName(testAssignment.getName());

        assertTrue(found.isPresent());
        assertEquals(testAssignment.getName(), found.get().getName());
    }
}