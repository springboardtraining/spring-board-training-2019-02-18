package integration.pa.spring.SpringBoard.repository;

import com.pa.spring.SpringBoard.SpringBoardApplication;
import com.pa.spring.SpringBoard.model.Skill;
import com.springboard.webapp.WebappApplication;
import com.springboard.webapp.model.Skill;
import com.springboard.webapp.repository.SkillRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

@DataJpaTest()
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringBoardApplication.class)
public class SkillRepositoryTest {
    @Autowired
    private SkillRepository skillRepository;
    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void findByNameLikeIgnoreCaseReturnsSkillWhenGivenExactName() {
        final Skill mySkill = new Skill("My Skill", "Skill Description");
        entityManager.persist(mySkill);
        entityManager.flush();

        Optional<Skill> found = skillRepository.findByNameLikeIgnoreCase(mySkill.getName());

        assertTrue(found.isPresent());
        assertEquals(mySkill.getName(), found.get().getName());
        assertEquals(mySkill.getDescription(), found.get().getDescription());
    }

    @Test
    public void findByNameLikeIgnoreCaseReturnsSkillWhenGivenLowerCaseName() {
        // given
        Skill mySkill = new Skill("MY UPPERCASE SKILL", "Skill Description");
        entityManager.persist(mySkill);
        entityManager.flush();

        Optional<Skill> found = skillRepository.findByNameLikeIgnoreCase(mySkill.getName().toLowerCase());

        assertTrue(found.isPresent());
        assertEquals(mySkill.getName(), found.get().getName());
    }

    @Test
    public void findByNameLikeIgnoreCaseReturnsSkillWhenGivenFirstCharacterOfName() {
        // given
        Skill mySkill = new Skill("My Skill", "Skill Description");
        entityManager.persist(mySkill);
        entityManager.flush();

        Optional<Skill> found = skillRepository.findByNameLikeIgnoreCase(mySkill.getName().substring(0, 1));

        assertTrue(found.isPresent());
        assertEquals(mySkill.getName(), found.get().getName());
    }
}